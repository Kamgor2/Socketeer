import 'reflect-metadata';
import { IControllerBase } from '../model/IControllerBase';

export const Controller = (namespace: string) => {
	return <any> ((target: IControllerBase, key) => {
		target.__namespace = namespace;
		return class extends target {
			constructor() {
				super();
				const services: string[] = Reflect.getOwnMetadata('design:paramtypes', target, key).map((a) => a.name);
				Reflect.ownKeys(this)
					.filter((propertyName: string) => {
						return !this[propertyName];
					})
					.forEach((propertyName: string) => {
						console.log(propertyName);
						this[propertyName] = services.shift();
					});
			}
		};
	});
};

// class TestService {}

// @Controller('asd')
// class Test {

// 	public someShit;

// 	constructor(
// 		private testService: TestService
// 	) {
// 		console.log('TEEEST');
// 		this.someShit = 'asd';
// 	}
// }

// const test = new Test(new TestService());
// console.log(test);
// console.log(test.someShit);
