import { IInjectableOptions } from './IInjectableOptions';
import { Scope } from './ScopeEnum';

export const Injectable = (options: IInjectableOptions) => {
	const { scope, asynchronous } = options;

	switch (scope) {
		case Scope.PER_MESSAGE:
			break;
		case Scope.PER_CONNECTION:
			break;
		case Scope.SINGLETON:
		default:
	}

	return (target) => {
		return target;
	};
};
