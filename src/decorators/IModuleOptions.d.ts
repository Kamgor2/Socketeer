import { IModuleBase } from '../model/IModuleBase';
import { IControllerBase } from '../model/IControllerBase';
import { IInjectableBase } from '../model/IInjectableBase';

export interface IModuleOptions {
    import: IModuleBase[];
    export: IModuleBase[];
    controllers: IControllerBase[];
    services: IInjectableBase[];
}